/**
  @page GlassLCD AN2656 GlassLCD Readme file
  
  @verbatim
  ******************** (C) COPYRIGHT 2009 STMicroelectronics *******************
  * @file GlassLCD/readme.txt 
  * @author   MCD Application Team
  * @version  V2.0.0
  * @date     04/27/2009
  * @brief    Description of the AN2656 "STM32F10xxx LCD glass driver firmware"                    
  ******************************************************************************
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  ******************************************************************************
   @endverbatim

@par Description

This AN describes a firmware driving a Glass LCD based on the STM32F10xxx GPIOs
peripherals. The main purpose of this firmware package is to provide resources 
facilitating the development of an application using a Glass LCD.

@par Directory contents 

  - inc 
    - GlassLCD/inc/stm32f10x_conf.h     Library Configuration file for the timer method
    - GlassLCD/inc/stm32f10x_it.h       Interrupt handlers header file for the timer method
    - GlassLCD/inc/glasslcd.h           Glass lcd Header file for the timer method
    - GlassLCD/inc/SystemConfig.h       System configuration header file for the timer method
    - GlassLCD/inc/glasslcd_RTC.h       Glass lcd Header file for the RTC method
    - GlassLCD/inc/SystemConfig_RTC.h   System configuration header file for the RTC method

  - src 
    - GlassLCD/src/stm32f10x_it.c       Interrupt handlers for the timer method
    - GlassLCD/src/glasslcd.c           Glass lcd firmware functions for the timer method
    - GlassLCD/src/SystemConfig.c       System configuration functions for the timer method
    - GlassLCD/src/main.c               Main program for the timer method
    - GlassLCD/src/stm32f10x_it_RTC.c   Interrupt handlers for the RTC method
    - GlassLCD/src/glasslcd_RTC.c       Glass lcd firmware functions for the RTC method
    - GlassLCD/src/SystemConfig_RTC.c   System configuration functions for the RTC method
    - GlassLCD/src/main_RTC.c           Main program for the RTC method


@par Hardware and Software environment
 
    - This example runs on STM32F10x High-Density, STM32F10x Medium-Density and
      STM32F10x Low-Density Devices.
    - This example has been tested with STMicroelectronics STM32 Low Power Demo 
      board.
    - Refer to the AN2656 pdf documentation, Section 7 (Hardware implementation).
      This demonstration can be easily tailored to any other hardware.

@par How to use it? 

In order to load the Glasslcd_Demo code, you have do the following:

 - EWARM: 
    - Open the Glass_LCD_RTC.eww or Glass_LCD_Timer.eww workspace
    - In the workspace toolbar select the project config:
        - STM32 Low power Demo board: to configure the project for STM32 High-density devices
    - Rebuild all files: Project->Rebuild all
    - Load project image: Project->Debug
    - Run program: Debug->Go(F5)

 - RIDE 
    - Open the Glass_LCD_RTC.rprj or Glass_LCD_Timer.rprj project
    - In the configuration toolbar(Project->properties) select the project config:
        - STM32 Low power Demo board: to configure the project for STM32 High-density devices
    - Rebuild all files: Project->build project
    - Load project image: Debug->start(ctrl+D)
    - Run program: Debug->Run(ctrl+F9)
@note
 - This project may have instabilities in debug mode with RIDE toolchain.

 - RVMDK 
    - Open the Glass_LCD_RTC.Uv2 or Glass_LCD_Timer.Uv2 project
    - In the build toolbar select the project config:  
        - STM32 Low power Demo board: to configure the project for STM32 High-density devices
    - Rebuild all files: Project->Rebuild all target files
    - Load project image: Debug->Start/Stop Debug Session
    - Run program: Debug->Run (F5)  

@note
 - Low-density devices are STM32F101xx and STM32F103xx microcontrollers where
   the Flash memory density ranges between 16 and 32 Kbytes.
 - Medium-density devices are STM32F101xx and STM32F103xx microcontrollers where
   the Flash memory density ranges between 32 and 128 Kbytes.
 - High-density devices are STM32F101xx and STM32F103xx microcontrollers where
   the Flash memory density ranges between 256 and 512 Kbytes.


 * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
 */
