/**
  ******************************************************************************
  * @file GlassLCD/inc/SystemConfig.h 
  * @author  MCD Application Team
  * @version  V2.0.0
  * @date  04/27/2009
  * @brief  System Configuration header file for timer method.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  */ 


#ifndef __SYSTEM_CONFIH_H
#define __SYSTEM_CONFIH_H
/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define RCC_APB1Periph_TIMx           RCC_APB1Periph_TIM3

#define RCC_APB1Periph_Used_LCD_GPIO  RCC_APB2Periph_GPIOC |\
                                      RCC_APB2Periph_GPIOD |\
                                      RCC_APB2Periph_GPIOE

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/ 
/* Private function prototypes -----------------------------------------------*/
void RCC_Configuration(void);
void GPIO_Configuration(void);
void NVIC_Configuration(void); 
void EXTI_Configuration(void);
void SystemConfiguration(void);
void TIMx_Configuration(TIM_TypeDef* TIMx);
	  
/* Private functions ---------------------------------------------------------*/
#endif /* __SYSTEM_CONFIH_H */

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
