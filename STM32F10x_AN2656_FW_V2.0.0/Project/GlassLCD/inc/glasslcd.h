/**
  ******************************************************************************
  * @file GlassLCD/inc/glasslcd.h 
  * @author  MCD Application Team
  * @version  V2.0.0
  * @date  04/27/2009
  * @brief  LCD glass configuration header file for timer method.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  */ 


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __GLASSLCD_H
#define __GLASSLCD_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "SystemConfig.h"


/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

/*----------------------- LCD user configuration -----------------------------*/
/* Incomment the define line of the used LCD and comment the other one */
#define USE_LCD_REF_PD_878    /* LCD ref: PD_878 : 8 digits 128 segments */
//#define USE_LCD_REF_CT4_098  /* LCD ref: CT4-098: 4 digits 64 segments */

/* Common lines configuration ----------------------------------------------- */
#define CommonLines_Port   GPIOC /* Port where the 4 common lines are connected */

#define CommonLines_EightHighPortPins  
                                /*  The previous line is to define where Common
                                    lines are located:
                                    - Uncomment this line: the 4 common lines  
                                      are located between PX.8 and PX.15
                                    - Comment this line: the 4 common lines  
                                      are located between PX.0 and PX.7
                                */

#define CommonLines_Pin_Offset   0   /* Pin offset (do not exceed 4: [0..4]) 
                                       (offset of eight low port pins or
                                       eight high port pins according to
                                       CommonLines_EightHighPortPins commented
                                       or not */

/* Configuration of the two GPIOs which drive the LCD (power on/off) -------- */
#define LCD_Bias_Port     GPIOC /* Port which the LCD bias pin is connected */


#define LCD_BiasPlus_Pin  GPIO_Pin_7  /* The number of the first GPIO (LCD bias plus)   
                                         which will power-off the resistor bridge */

/* Segment lines configuration ---------------------------------------------- */
/* Port where the Low Quarter digits segment lines are connected */
#define SegmentsLines_LQD_Port   GPIOE  

/* Port where the High Quarter digits segment lines are connected */
#define SegmentsLines_HQD_Port   GPIOD  

/* Define of used TIM, GPIOs ports and the TIM interrupt channel -------------*/
#define TIMER_LCD                 TIM3 /* Timer used to drive tle LCD glass */
#define TIM_LCD_IRQChannel        TIM3_IRQn /* The used interrupt channel */
#define RCC_APB1Periph_TIMx       RCC_APB1Periph_TIM3 /* Activate the used TIM clock */
/* Enable the used GPIOs clocks */
#define RCC_APB2Periph_Used_GPIO    RCC_APB2Periph_GPIOC \
                                  | RCC_APB2Periph_GPIOD \
                                  | RCC_APB2Periph_GPIOE 

/* LCD interrupt priority configuration ------------------------------------- */
#define LCD_Priority_Value       3

/* If the LCD interrupt is run always as priority 0 please comment this line */
#define LCD_Use_Boost_Priority 

/* LCD contarst value */
#define LCD_DefaultContrast    0x100 


/*------ GPIOs Masks definitions (section not modifiable by the user) --------*/
#ifdef  CommonLines_EightHighPortPins
  #define PinsConfig         CRH
  #define ALL_COMMON_LINES  (uint16_t)(0x000F << (CommonLines_Pin_Offset+8))
#else
  #define PinsConfig         CRL
  #define ALL_COMMON_LINES  (uint16_t)(0x000F << CommonLines_Pin_Offset)
#endif

#define ALL_COMMON_LINES_PP   (0x3333 << (CommonLines_Pin_Offset * 4))

#define ALL_SEGMENT_LINES      0xFFFF
#define ALL_SEGMENT_LINES_PP   0x33333333

/* Exported macro ------------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */
void Convert (char* c,uint8_t point);
void LCD_WriteChar (char* car, uint8_t point,uint8_t pos);
void LCD_WriteString (char* string);
void LCD_GPIO_Coms_Masks_Init(void);
uint32_t OffsetValue(uint8_t Pin_offset);
void LCD_SettingContrast(uint16_t LCD_ContrastValue);
void LCD_GPIO_Init(void);

#endif /* __GLASSLCD_H */

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
