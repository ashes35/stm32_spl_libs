/**
  ******************************************************************************
  * @file GlassLCD/src//main.c 
  * @author  MCD Application Team
  * @version  V2.0.0
  * @date  04/27/2009
  * @brief  Main program body using Timer method and Sleep mode
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  */ 


/* Includes ------------------------------------------------------------------*/
#include "glasslcd.h"


/** @addtogroup GlassLCD
  * @{
  */ 


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/ 
uint32_t DebugDelay;  
__IO uint32_t i;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/**
  * @brief  Main program
  * @param  : None.
  * @retval : None.
  */
int main(void)
{


  /* Enable Debug in low power modes in the DBGMCU_CR register */
  * (__IO uint32_t *) 0xE0042004 = 0x00000007;
  
  /* Debug delay */
  for (DebugDelay=1000000; DebugDelay!=0; DebugDelay--);    
  
  /* System configuration */
  SystemConfiguration();

  /* Power on the resistor bridge */
  /* LCD_Bias = VDD */
  GPIO_SetBits(LCD_Bias_Port, LCD_BiasPlus_Pin);

  /* Set LCD contrast */
  LCD_SettingContrast(LCD_DefaultContrast); 

  /* Show "STM32" text */
  LCD_WriteString("STM32");
  
  while(1)
  {
    /* Enter in Sleep Mode */
    __WFI(); 
  }
}

#ifdef USE_FULL_ASSERT


/**
  * @brief  Reports the name of the source file and the source line number
  * where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval : None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
   
  /* Infinite loop */
  while (1)
  {
  }
}
#endif
/**
  * @}
  */ 



/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
