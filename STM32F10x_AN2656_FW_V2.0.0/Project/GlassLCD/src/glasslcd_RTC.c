/**
  ******************************************************************************
  * @file GlassLCD/src/glasslcd_RTC.c 
  * @author  MCD Application Team
  * @version  V2.0.0
  * @date  04/27/2009
  * @brief  LCD glass driver file using RTC method and STOP mode
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  */ 


/* Includes ------------------------------------------------------------------*/
#include "glasslcd_RTC.h"


/** @addtogroup GlassLCD
  * @{
  */ 


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#ifdef USE_LCD_REF_CT4_098 
 #define NumberOfUsedDigits 4  /* For CT4_098 LCD reference */
#else
 #define NumberOfUsedDigits 8  /* For PD-878 LCD reference */
#endif

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Global variables definition and initialization ----------------------------*/
uint16_t  SegmentsValues_Lower_Quarter_Digits[4]; /* LCD frame buffer for low 
                                               quarter digits (digits 1 -> 4) */
uint16_t  SegmentsValues_Higher_Quarter_Digits[4]; /* LCD frame buffer for high 
                                                quarter digits (digits 5 -> 8) */
uint16_t  digit[4];     /* Digit frame buffer */

#ifdef CommonLines_EightHighPortPins
const  uint16_t  CommonLine[4]={
                             1<<(CommonLines_Pin_Offset+8),
                             2<<(CommonLines_Pin_Offset+8),
                             4<<(CommonLines_Pin_Offset+8),
                             8<<(CommonLines_Pin_Offset+8)
                          };
#else
const  uint16_t  CommonLine[4]={
                             1<<CommonLines_Pin_Offset,
                             2<<CommonLines_Pin_Offset,
                             4<<CommonLines_Pin_Offset,
                             8<<CommonLines_Pin_Offset
                          };
#endif

/* GPIOs to be configured to VDD/2 */
uint32_t  CommonLine_VDD_2[4]={ 
                           0xFFFF000F,
                           0xFFFF00F0,
                           0xFFFF0F00,
                           0xFFFFF000
                          };

/* GPIOs to be configured to Output PP */
uint32_t  CommonLine_OUT_PP[4]={
                            0x00000003,
                            0x00000030,
                            0x00000300,
                            0x00003000
                           };


/*  =========================================================================
                                 LCD MAPPING
    =========================================================================

               A
      --  ----------
    X \/  |\   |I  /|
         F| H  |  J |B
          |  \ | /  |
          --G-- --K--
          |   /| \  |
        E |  L |  N |C
          | /  |M  \|   _
          -----------  | |DP   
              D         -

A LCD character coding is based on the following matrix:

                       ===========================
                        The PD-878 LCD matrix map:
                       ===========================

{ X , F , E , D  }
{ I , J , K , N  }
{ A , B , C , DP }
{ H , G , L , M  }

The characher A for example is:
-------------------------------    
 { 0 , 1 , 1 , 0 }
 { 0 , 0 , 1 , 0 }
 { 1 , 1 , 1 , 0 }
 { 0 , 1 , 0 , 0 }
-------------------
=  4   D   7   0   hex

=> 'A' = 0x4D70  


                       ============================
                        The CT4-098 LCD matrix map:
                       ============================

{ X , I , A , DP }
{ F , J , K , B  }
{ E , G , H , C  }
{ M , L , N , D  }

The characher A for example is:
-------------------------------   
 { 0 , 1 , 1 , 0 }
 { 0 , 0 , 1 , 0 }
 { 1 , 0 , 1 , 0 }
 { 0 , 1 , 1 , 0 }
-------------------
=  4   9   F   0   hex

=> 'A' = 0x49F0  

*/

/*  =========================================================================
                       LETTERS AND NUMBERS MAPPING DEFINITION
    ========================================================================= */

#ifdef USE_LCD_REF_PD_878  /* Letters and number map of PD_878 LCD */
const uint16_t LetterMap[26]=
   { 
     /* A      B      C      D      E      F      G      H      I  */
     0x4D70,0x6469,0x4111,0x6449,0x4911,0x4910,0x4171,0x0D70,0x6009,
     /* J      K      L      M      N      O      P      Q      R  */
     0x0451,0x0B12,0x0111,0x8750,0x8552,0x4551,0x4D30,0x4553,0x4D32,
     /* S      T      U      V      W      X      Y      Z  */     
     0x4961,0x6008,0x0551,0x0390,0x05D2,0x8282,0x8208,0x4281 
   };

const uint16_t NumberMap[10]=
    { 
      /* 0      1      2      3      4      5      6      7      8      9  */
      /*0x4551*/ 0x47D1,0x0640,0x4C31,0x4C61,0x0D60,0x4961,0x4971,0x4440,0x4D71,0x4D61
    };
#endif  


#ifdef USE_LCD_REF_CT4_098  /* Letters and number map of CT4_098 LCD */
const uint16_t LetterMap[26]=
   {
     /* A      B      C      D      E      F      G      H      I  */
     0x49F0,0x01F8,0x4118,0x08F8,0x4178,0x4170,0x41D8,0x09F0,0x600A,
     /* J      K      L      M      N      O      P      Q      R  */
     0x0888,0x0534,0x0118,0x0F90,0x0B94,0x4998,0x4970,0x499C,0x4974,
     /* S      T      U      V      W      X      Y      Z  */     
     0x41E8,0x6002,0x0998,0x0511,0x299A,0x0605,0x0601,0x4409
   };

const uint16_t NumberMap[10]=
    { /* 0      1      2      3      4      5      6      7      8      9  */
      0x4998,0x0880,0x4878,0x48E8,0x09E0,0x41E8,0x41F8,0x4880,0x49F8,0x49E8
    };
#endif

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/**
  * @brief  Initialize GPIOs to set segments lines and common line
  *   values to 0.
  * @param ne.
  * @retval : None.
  */
void LCD_GPIO_Init(void)
{
  /* Compute and load the GPIOs masks of the Common lines */
  LCD_GPIO_Coms_Masks_Init();
    
  /* All common lines = 0 */
  CommonLines_Port->ODR &= ~ALL_COMMON_LINES;
    
  /* All segment lines = 0 ( all SegmentsLines_LQD_Port pins = 0 ) */
  GPIO_Write(SegmentsLines_LQD_Port, (uint16_t)~ALL_SEGMENT_LINES);
    
#ifdef USE_LCD_REF_PD_878 
  GPIO_Write(SegmentsLines_HQD_Port, (uint16_t)~ALL_SEGMENT_LINES);
#endif          
   
  /* Configure all common lines on CommonLines port pins as Out_PP */    
  CommonLines_Port->PinsConfig &= (0xFFFF0000 << ((CommonLines_Pin_Offset) * 4)) \
                                   + OffsetValue(CommonLines_Pin_Offset);
  CommonLines_Port->PinsConfig |= ALL_COMMON_LINES_PP;
    
  /* Configure all segment lines of Lower Quarter digits as Out_PP */ 
  SegmentsLines_LQD_Port->CRL = ALL_SEGMENT_LINES_PP;
  SegmentsLines_LQD_Port->CRH = ALL_SEGMENT_LINES_PP;
       
#ifdef USE_LCD_REF_PD_878 
  /* Configure all segment lines of Higher Quarter digits as Out_PP */ 
  SegmentsLines_HQD_Port->CRL = ALL_SEGMENT_LINES_PP;
  SegmentsLines_HQD_Port->CRH = ALL_SEGMENT_LINES_PP;
#endif 
}



/**
  * @brief  computes the offset to add to the GPIO masks
  * @param Pin_offset: pin offset.
  * @retval : None.
  */
uint32_t OffsetValue(uint8_t Pin_offset)
{
  uint8_t i;
  uint32_t offset_Value = 1;

    for (i=0; i<Pin_offset; i++)
    {
      offset_Value =  offset_Value * 16;
    }
    
  return (offset_Value - 1);
}



/**
  * @brief  initilizes the LCD GPIOs masks.
  * @param ne.
  * @retval : None.
  */
void LCD_GPIO_Coms_Masks_Init(void)
{
  uint8_t i;
  
  for (i=0; i<4; i++)
  {
    CommonLine_VDD_2[i] = (CommonLine_VDD_2[i] << ((CommonLines_Pin_Offset) * 4))\
                           + OffsetValue(CommonLines_Pin_Offset);
    CommonLine_OUT_PP[i] = CommonLine_OUT_PP[i] << ((CommonLines_Pin_Offset) * 4);
  }
}


/**
  * @brief  converts an ascii char to the a LCD digit (previous coding)
  * @param c: a char to display.
  * @param point: flag indicating if a point has to be add in front of
  *   displayed character (0: no, 1: yes)
  * @retval : None.
  */
void Convert(char* c,uint8_t point)
{ 
  uint16_t car=0,tmp;
  uint8_t i;
  const uint16_t mask[4]={0xF000,0x0F00,0x00F0,0x000F};
  const uint8_t shift[3]={12,8,4};

  if ((*c<0x5B)&(*c>0x40))
  {
    car = LetterMap[*c-0x41];
  }
  if ((*c<0x3A)&(*c>0x2F)) 
  {
    car = NumberMap[*c-0x30];
  }
  if (*c==0x20)
  {
    car =0x00;
  }
  
#ifdef USE_LCD_REF_PD_878   
  if (point==1)
  {
    car|=0x0004;
  }
#endif

#ifdef USE_LCD_REF_CT4_098   
  if (point==1)
  {
    car|=0x8000;
  }
#endif
  
  for (i=0;i<3;i++)
  {
    tmp = car&mask[i];
    digit[i] = tmp>>shift[i];
  }
  
  digit[3] = (car&mask[3]); 
}



/**
  * @brief  This function writes a char in the LCD frame buffer
  * @param car: the caracter to dispaly.
  * @param point: a point to add in front of char
  *   0: no point to add in front of char.
  *   1: add a point in front of char.  
  * @param position: position in the LCD of the caracter to write. 
  *   it can be: [0:3] (in case of CT4-098 LCD glass)
  *   [0:7] (in case of PD_878 LCD glass)
  * @retval : None.
  */
void LCD_WriteChar(char* car,uint8_t point,uint8_t position)
  {
   uint8_t i;
   const uint16_t mask[4]={0x000F,0x00F0,0x0F00,0xF000};
   uint16_t Digits_Lower_Quarter[4];
   
#ifdef USE_LCD_REF_PD_878    
   uint16_t Digits_Higher_Quarter[4];
#endif    
   Convert(car,point);
  
   switch(position)
   {
     /* Position 0 on LCD (Digit1)*/
     case 0: for (i=0;i<4;i++)
             {
               Digits_Lower_Quarter[i] = digit[i];
#ifdef USE_LCD_REF_PD_878                 
               Digits_Higher_Quarter[i]=0;
#endif                
              }
              break;
             
     /* Position 1 on LCD (Digit2)*/
     case 1: for (i=0;i<4;i++)
             {
               Digits_Lower_Quarter[i] =(digit[i]<<4);
#ifdef USE_LCD_REF_PD_878                 
               Digits_Higher_Quarter[i]=0;
#endif               
             }
             break;
             
     /* Position 2 on LCD (Digit3)*/
     case 2: for (i=0;i<4;i++)
             {
               Digits_Lower_Quarter[i]=(digit[i]<<8);
#ifdef USE_LCD_REF_PD_878                 
               Digits_Higher_Quarter[i]=0;
#endif                
             }
             break;
 
     /* Position 3 on LCD (Digit4)*/
     case 3: for (i=0;i<4;i++)
             {
               Digits_Lower_Quarter[i]=(digit[i]<<12);
#ifdef USE_LCD_REF_PD_878               
               Digits_Higher_Quarter[i]=0;
#endif               
             }
             break;

#ifdef USE_LCD_REF_PD_878   /* Higher quarter digits in LCD (position from 4 to 7) */
             
     /* Position 4 on LCD (Digit5)*/
     case 4: for (i=0;i<4;i++)
             {
               Digits_Higher_Quarter[i]= digit[i];
               Digits_Lower_Quarter[i]=0;
             }
             break;
             
     /* Position 5 on LCD (Digit6)*/
     case 5: for (i=0;i<4;i++)
             {
               Digits_Higher_Quarter[i]=(digit[i]<<4);
               Digits_Lower_Quarter[i]=0;
             }
             break;
             
     /* Position 6 on LCD (Digit7)*/
     case 6: for (i=0;i<4;i++) 
             {
               Digits_Higher_Quarter[i]=(digit[i]<<8);
               Digits_Lower_Quarter[i]=0;
             }
             break;
             
     /* Position 7 on LCD (Digit8)*/
     case 7: for (i=0;i<4;i++)
             {
               Digits_Higher_Quarter[i]=(digit[i]<<12);
               Digits_Lower_Quarter[i]=0;
             }
             break;
#endif             
             
     default:break;
    }

   if (position < 4) /* Segments of lower quarter digits in LCD (position from 4 to 7) */
   {
     for (i=0;i<4;i++)
     {
       SegmentsValues_Lower_Quarter_Digits[i] = \
      (SegmentsValues_Lower_Quarter_Digits[i]&~mask[position])| Digits_Lower_Quarter[i];
                                                   
     }
   }
#ifdef USE_LCD_REF_PD_878    
   else  /* Segments of higher quarter digits in LCD (position from 4 to 7) */
   { 
     for (i=0;i<4;i++)
     {
       SegmentsValues_Higher_Quarter_Digits[i] = \
      (SegmentsValues_Higher_Quarter_Digits[i]&~mask[position-4])| Digits_Higher_Quarter[i];
                                                  
     }
   }
#endif
   
  }




/**
  * @brief  This function writes a string in the LCD
  * @param str: string to dispaly in the LCD.
  * @retval : None.
  */
void LCD_WriteString(char* str)
{
  uint8_t i;
  
  for (i=0;i<NumberOfUsedDigits;i++)
  {
    LCD_WriteChar(str+i,0,i);
  }
} 

/**
  * @}
  */ 


/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
