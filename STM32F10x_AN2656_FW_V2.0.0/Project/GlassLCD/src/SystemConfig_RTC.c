/**
  ******************************************************************************
  * @file GlassLCD/src/SystemConfig_RTC.c 
  * @author  MCD Application Team
  * @version  V2.0.0
  * @date  04/27/2009
  * @brief  System configuration driver  for STOP method 
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  */ 


/* Includes ------------------------------------------------------------------*/
#include "glasslcd_RTC.h"



/** @addtogroup GlassLCD
  * @{
  */ 


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
GPIO_InitTypeDef GPIO_InitStructure;
EXTI_InitTypeDef EXTI_InitStructure;
NVIC_InitTypeDef NVIC_InitStructure;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/**
  * @brief  Configures the system.
  * @param None.
  * @retval : None.
  */
void SystemConfiguration(void)
{
  /* System Clocks Configuration */
  RCC_Configuration();
 
  /* Configure EXTI Line17(RTC Alarm) and EXTI Line9 */
  EXTI_Configuration();
  
  /* Configure the GPIOs */
  GPIO_Configuration();
  
  /* RTC Configuration */
  RTC_Configuration();
  
  /* Inisialize RTC to drive LCD */
  RTC_Init();
  
  /* NVIC configuration */
  NVIC_Configuration();
}
  


/**
  * @brief  Configures the different system clocks.
  * @param  None
  * @retval : None
  */
void RCC_Configuration(void)
{
  /* RCC system reset(for debug purpose) */
  RCC_DeInit();

  /* HCLK = SYSCLK = HSI/4 = 8MHz / 4 = 2MHz */
  RCC_HCLKConfig(RCC_SYSCLK_Div4);

  /* PCLK2 = HCLK = 2MHz */
  RCC_PCLK2Config(RCC_HCLK_Div1); 

  /* PCLK1 = HCLK = 2MHz */
  RCC_PCLK1Config(RCC_HCLK_Div1);

  /* Flash 0 wait state */
  FLASH_SetLatency(FLASH_Latency_0);

  /* Enable Flash half cycle */
  FLASH_HalfCycleAccessCmd(FLASH_HalfCycleAccess_Enable);

  /* Enable Prefetch Buffer */
  FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);

  /* Select HSI as system clock source */
  RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI);

  /* Enable PWR clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

  /* GPIOx and AFIO clocks enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_Used_GPIO | RCC_APB2Periph_AFIO, ENABLE);
}



/**
  * @brief  Configures NVIC and Vector Table base location.
  * @param  None
  * @retval : None
  */
void NVIC_Configuration(void)
{
 
  /* 2 bits for Preemption Priority and 2 bits for Sub Priority */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

  /* Enable the RTC Alarm Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = RTCAlarm_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = LCD_Priority_Value;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  /* Enable the EXTI0 Interrupt: LCD button On/Off */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);  
}



/**
  * @brief  Configures the different GPIO ports.
  * @param  None
  * @retval : None
  */
void GPIO_Configuration(void)
{
  /* Disable the Serial Wire Jtag Debug Port SWJ-DP to minimize power consumption */
  GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable, ENABLE);

  /* Configure all GPIOs as AIN */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
  GPIO_Init(GPIOC, &GPIO_InitStructure);
  GPIO_Init(GPIOD, &GPIO_InitStructure);
  GPIO_Init(GPIOE, &GPIO_InitStructure);

  /* Configure PA.00 as input floating (EXTI Line0): LCD button On/Off */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* Connect EXTI Line0 to PA.00 */
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource0);

  /* Configure LCD_BiasPlus as Out Push-Pull */
  GPIO_InitStructure.GPIO_Pin = LCD_BiasPlus_Pin;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(LCD_Bias_Port, &GPIO_InitStructure);
  
  /* GPIOs initialization: all segments and common lines are set as out PP and
     reset to 0  */
  LCD_GPIO_Init();
}



/**
  * @brief  Configures the RTC.
  * @param  None
  * @retval : None
  */
void RTC_Configuration(void)
{
  /* Allow access to BKP Domain */
  PWR_BackupAccessCmd(ENABLE);

  /* Disable LSE */
  RCC_LSEConfig(RCC_LSE_OFF);

  /* Enable LSI */  
  RCC_LSICmd(ENABLE);

  /* Wait till LSI is ready */
  while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
  {
  }

  /* Select LSI as RTC Clock Source */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);

  /* Enable RTC Clock */
  RCC_RTCCLKCmd(ENABLE);

  /* Wait for RTC registers synchronization */
  RTC_WaitForSynchro();

  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
}



/**
  * @brief  Initializes RTC to drive LCD.
  * @param  None
  * @retval : None
  */
void RTC_Init(void)
{
  /* Enable the RTC Alarm */
  RTC_ITConfig(RTC_IT_ALR , ENABLE);

  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
 
  /* Set RTC prescaler */
  RTC_SetPrescaler(3); /* RTC period = RTCCLK/RTC_PR = (40KHz)/(3+1) = 10KHz */

  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
  
  /* Reset RTC Counter */
  RTC_SetCounter(0x0);
    
  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
    
  /* Set the next time of alarm interrupt occur */
  RTC_SetAlarm(PulseValueForContrast);
    
  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
    
  /* Clear reset flags */
  RCC_ClearFlag();
}  



/**
  * @brief  Configures EXTI Line9 and Line17(RTC Alarm).
  * @param  None
  * @retval : None
  */
void EXTI_Configuration(void)
{
  /* Configure EXTI Line17(RTC Alarm) to generate an interrupt on rising edge */
  EXTI_ClearITPendingBit(EXTI_Line17);
  EXTI_InitStructure.EXTI_Line = EXTI_Line17;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);    
  
  /* Configure EXTI Line0 to generate an interrupt on falling edge: 
     for LCD button On/Off */  
  EXTI_InitStructure.EXTI_Line = EXTI_Line0;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure); 
}
 
/**
  * @}
  */ 



/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
