/**
  ******************************************************************************
  * @file GlassLCD/src/stm32f10x_it.c 
  * @author  MCD Application Team
  * @version  V2.0.0
  * @date  04/27/2009
  * @brief  Main Interrupt Service Routines.
  *         This file provides template for all exceptions handler and 
  *         peripherals interrupt service routine.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_it.h"
#include "glasslcd.h"

/** @addtogroup GlassLCD
  * @{
  */ 


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern uint16_t SegmentsValues_Lower_Quarter_Digits[4];
extern uint16_t SegmentsValues_Higher_Quarter_Digits[4];
extern const uint16_t CommonLine[4];
extern uint32_t CommonLine_OUT_PP[4];
extern uint32_t CommonLine_VDD_2[4];
uint8_t CC2_1_Phase_Execute=0;
uint16_t lcdcr=0;
uint8_t LCDPowerOff=0;
uint32_t nDelay;
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M3 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval : None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval : None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval : None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval : None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval : None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval : None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval : None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval : None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval : None
  */
void SysTick_Handler(void)
{
}

/**
  * @brief  This function handles EXTI0_IRQHandler .
  * @param  None
  * @retval : None
  */
void EXTI0_IRQHandler(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;

  /* Insert delay to avoid the button bouncing effect */
  for(nDelay=0; nDelay<0x2FFF; nDelay++);

  if(EXTI_GetITStatus(EXTI_Line0) != RESET)
  {
    /* Clear EXTI pending bit */ 
    EXTI_ClearITPendingBit(EXTI_Line0);
    
    if(LCDPowerOff==1)
    { 
     /* LCD TIM conter desable */
     TIM_Cmd(TIMER_LCD, DISABLE);
      
     /* Enable the TIMx global Interrupt */
     NVIC_InitStructure.NVIC_IRQChannel = TIM_LCD_IRQChannel;
     NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
     NVIC_Init(&NVIC_InitStructure); 

     /* All Common lines = 0 */
     CommonLines_Port->ODR &= ~ALL_COMMON_LINES;
      
     /* Configure all Common lines on CommonLines port pins as Out_PP */    
     CommonLines_Port->PinsConfig |= ALL_COMMON_LINES_PP;
      
     /* All Segment lines = 0 ( all SegmentsLines_LQD_Port pins = 0 ) */
     SegmentsLines_LQD_Port->ODR = (uint16_t)~ALL_SEGMENT_LINES;

  #ifdef USE_LCD_REF_PD_878 
     /* All SegmentsLines_LQD_Port pins = 0 */
     SegmentsLines_HQD_Port->ODR = (uint16_t)~ALL_SEGMENT_LINES;
  #endif  
     /* LCD Bias Plus Pin = 0V (LCD Bias Minus Pin is always in 0V level) */
     GPIO_ResetBits(LCD_Bias_Port, LCD_BiasPlus_Pin);
        
     LCDPowerOff=0;
    }
    
    else
    { 
     /* LCD TIM conter enable */
     TIM_Cmd(TIMER_LCD, ENABLE); 
      
     /* Enable the TIMx global Interrupt */
     NVIC_InitStructure.NVIC_IRQChannel = TIM_LCD_IRQChannel;
     NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
     NVIC_Init(&NVIC_InitStructure); 

     /* Power on the resistor bridge ( LCD Bias Minus is always set to 0) */
     GPIO_SetBits(LCD_Bias_Port, LCD_BiasPlus_Pin);
   
     LCDPowerOff=1;
   }
 }
}
/**
  * @brief  This function handles TIM3_IRQHandler .
  * @param  None
  * @retval : None
  */
void TIM3_IRQHandler(void)
{
  /* If the user uses an another timer other than TIM3, he has to move all this 
  code to the specified TIMx_IRQHandler routine and change the define above 
  from TIM3 to the specified timer ex: TIM2 */  
  
/* The output compare 1 interrupt event --------------------------------------*/
  if ((TIMER_LCD->SR & 0x0002) != RESET)
  {

    /* Clear the CC1 flag */
    TIMER_LCD->SR &= (uint16_t)~0x0002;

#ifdef LCD_Use_Boost_Priority  
    /* Boost the LCD priority to 0 */
    __disable_irq();
#endif
    
    /* All Segment lines = 0 ( all SegmentsLines_LQD_Port pins = 0 ) */
    SegmentsLines_LQD_Port->ODR = (uint16_t)~ALL_SEGMENT_LINES;
    
#ifdef USE_LCD_REF_PD_878 
    SegmentsLines_HQD_Port->ODR = (uint16_t)~ALL_SEGMENT_LINES;
#endif  
    /* All Common lines = 0 */
    CommonLines_Port->ODR &= ~ALL_COMMON_LINES;
    
    /* Configure all Common lines on CommonLines port pins as Out_PP */    
    CommonLines_Port->PinsConfig |= ALL_COMMON_LINES_PP;

#ifdef LCD_Use_Boost_Priority 
    /* Return to the previous LCD priority interrupt */
    __enable_irq();
#endif

  }

/* The output compare 2 interrupt event --------------------------------------*/  
  else if ((TIMER_LCD->SR & 0x0004) != RESET)  
  {      
    /* Clear the CC2 flag */
    TIMER_LCD->SR &= (uint16_t)~0x0004;
      
 /* CC2_1 phase --------------------------------------------------------------*/    
    if (CC2_1_Phase_Execute == 0)       
     {
      

#ifdef LCD_Use_Boost_Priority 
    /* Boost the LCD priority to 0 */       
    __disable_irq(); 
#endif

       /* Segments[lcdcr] to be turned on are loaded with the value 1 otherwise 0 */
       SegmentsLines_LQD_Port->ODR =SegmentsValues_Lower_Quarter_Digits[lcdcr];

#ifdef USE_LCD_REF_PD_878        
       SegmentsLines_HQD_Port->ODR =SegmentsValues_Higher_Quarter_Digits[lcdcr];
#endif 
       /* Common_line[lcdcr] is set to low */
       CommonLines_Port->BRR = CommonLine[lcdcr];
       
       /* Other Common lines set to Vdd/2 */
       CommonLines_Port->PinsConfig &= CommonLine_VDD_2[lcdcr];
       
       /* Set Common_line[lcdcr] out push pull */
       CommonLines_Port->PinsConfig |= CommonLine_OUT_PP[lcdcr];

#ifdef LCD_Use_Boost_Priority
    /* Return to the previous LCD priority interrupt */       
    __enable_irq(); 
#endif        
       CC2_1_Phase_Execute++;

    }

 /* CC2_2 phase --------------------------------------------------------------*/    
   else 
   {  

#ifdef LCD_Use_Boost_Priority  
     /* Boost the LCD priority to 0 */     
     __disable_irq();
#endif 
     
     /* Segments(lcdcr) values are inverted */ 
     SegmentsLines_LQD_Port->ODR = ~SegmentsValues_Lower_Quarter_Digits[lcdcr];
     
#ifdef USE_LCD_REF_PD_878      
     SegmentsLines_HQD_Port->ODR = ~SegmentsValues_Higher_Quarter_Digits[lcdcr];
#endif 
     
     /* Common_line[lcdcr] is set to high */
     CommonLines_Port->BSRR = CommonLine[lcdcr];
     
     /* Other Common lines set to Vdd/2 */
     CommonLines_Port->PinsConfig &= CommonLine_VDD_2[lcdcr];
     
     /* Other Common lines out push pull */
     CommonLines_Port->PinsConfig |= CommonLine_OUT_PP[lcdcr];
    
#ifdef LCD_Use_Boost_Priority 
     /* Return to the previous LCD priority interrupt */     
     __enable_irq(); 
#endif 
     
     lcdcr++;
      
     if(lcdcr>3)
     {
       lcdcr =0;
     }
     
     CC2_1_Phase_Execute = 0;
   }
  } 
}

/**
  * @brief  This function handles RTCAlarm_IRQHandler .
  * @param  None
  * @retval : None
  */
void RTCAlarm_IRQHandler(void)
{
}
/******************************************************************************/
/*                 STM32F10x Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (GlassLCD), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f10x_xx.s).                                            */
/******************************************************************************/

/**
  * @brief  This function handles GlassLCD interrupt request.
  * @param  None
  * @retval : None
  */
/*void GlassLCD_IRQHandler(void)
{
}*/



/**
  * @}
  */ 


/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
