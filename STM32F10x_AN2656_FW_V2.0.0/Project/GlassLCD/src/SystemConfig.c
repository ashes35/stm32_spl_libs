/**
  ******************************************************************************
  * @file GlassLCD/src/SystemConfig.c 
  * @author  MCD Application Team
  * @version  V2.0.0
  * @date  04/27/2009
  * @brief  System configuration driver for Timer method 
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  */ 


/* Includes ------------------------------------------------------------------*/
#include "glasslcd.h"



/** @addtogroup GlassLCD
  * @{
  */ 


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/ 
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/**
  * @brief  Configures the system.
  * @param None.
  * @retval : None.
  */
void SystemConfiguration(void)
{
  /* System Clocks Configuration */
  RCC_Configuration();

  /* NVIC Configuration */
  NVIC_Configuration();
  
  /* GPIO configuration */
  GPIO_Configuration();

  /* Configure EXTI Line0 to generate an interrupt on falling edge */
  EXTI_Configuration();

  /* Timer configuration */
  TIMx_Configuration(TIMER_LCD);
}
  


/**
  * @brief  Configures the different system clocks.
  * @param None.
  * @retval : None.
  */
void RCC_Configuration(void)
{   
  /* RCC system reset(for debug purpose) */
  RCC_DeInit();

  /* HCLK = SYSCLK/4 = 2MHz */
  RCC_HCLKConfig(RCC_SYSCLK_Div4); 
  
  /* PCLK2 = HCLK = 2MHz */
  RCC_PCLK2Config(RCC_HCLK_Div1); 

  /* PCLK1 = HCLK = 2MHz */
  RCC_PCLK1Config(RCC_HCLK_Div1);
    
  /* Flash 2 wait state */
  FLASH_SetLatency(FLASH_Latency_0);
    
  /* Enable Flash half cycle */
  FLASH_HalfCycleAccessCmd(FLASH_HalfCycleAccess_Enable);

  /* Enable Prefetch Buffer */
  FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);
    
  /* Select HSI as system clock source */
  RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI);
    
  /* TIM to be used clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIMx, ENABLE);

  /* GPIOx clocks enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_Used_GPIO | RCC_APB2Periph_AFIO, ENABLE);

}



/**
  * @brief  Configure the timer x to drive the LCD.
  * @param TIMx: timer to be used to drive the LCD.
  * @retval : None.
  */
void TIMx_Configuration(TIM_TypeDef* TIMx)
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  TIM_OCInitTypeDef TIM_OCInitStructure;
  
  /* TIMx is used in Output Compare Toggle Mode */
  TIM_TimeBaseStructure.TIM_Period = 0x1CB8;          
  TIM_TimeBaseStructure.TIM_Prescaler = 0x00;       
  TIM_TimeBaseStructure.TIM_ClockDivision = 0x0;    
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; 
  TIM_TimeBaseInit(TIMER_LCD, &TIM_TimeBaseStructure);
  
  /* Prescaler configuration: 
     TIMxCLK = PCLK1 x 1 = 2MHz ( x 1 because the APB prescaler is set to 1  
     otherwise TIMxCLK = PCLK1 x 2 refer to '4.2 Clocks' section in the Reference
     Manual) the Timer Prescaler is set to 1  (Prescaler + 1 = 0 + 1 = 1) 
     -> the TIMx counter clock = TIMxCLK / 1 = 2MHz */
   TIM_PrescalerConfig(TIMER_LCD, 0, TIM_PSCReloadMode_Immediate); /* Prescaler = 0 */
    
    
   /* The LCD display frequency range is 30 - 200Hz typically. To have minimum
      load CPU, the minimum LCD frequency is recommanded. For example 34Hz(29.4ms)   
      can be choosed. So the period will have a value 29.4/8 = 3.67ms (272Hz)
      
      TIMx in Toggle mode:
       ====================
         <---- Period = 3.67ms ----->
          __________________________                            _______________
         |                          |                          |
       __|                          |__________________________|
         
         |       <---- Period = 3.67ms ----->
         |        __________________________                            _______
         |       |                          |                          |
       __________|                          |__________________________|
         |
         |<-CC1->|
  
         |           <---- Period = 3.67ms ----->
         |            __________________________                            ___
         |           |                          |                          |
       ______________|                          |__________________________|
  
         |<--CC2---->|    
     
    Period = TIMx counter clock / TIM_Period = 272Hz (3.67ms)
    -> TIM_Period = 2MHz / 272Hz = 0x1CB8

  CC1 and CC2 will have the following values:  
  - CC1 = 0x0 (TIM_Pulse channel 1 value, still always unchanged, CC1 = 0) 
  - CC2 = 0x1CB8 (TIM_Pulse channel 2 value)
  CC1 and CC2 will have theses forms:
  
         <----- Period = 3.67ms ---->
         <-- AutoReload = 0x1CB8 --->
          __________________________                            _______________
         |                          |                          |
       __|                          |__________________________|
         
         <----- Period = 3.67ms ---->
          __________________________                            _______________
         |                          |                          |
      ___|                          |__________________________|
  
      CC1=0
  
         |           <----- Period = 3.67ms ---->
         |            __________________________                            ___
         |           |                          |                          |
       ______________|                          |__________________________|
  
         |<--CC2---->|
          = 0x1CB8 (LCD_DefaultContrast), when this value changes, the delay  
            between CC2 and CC1 event changes. This delay corresponds to the  
            Common line signal active time which controls the LCD contrast.

  
  Common line signals timings:
  ----------------------------
  One full cycle = Period x 8 = 3.67ms x 8 = 29.36 ms (34Hz)
  
  
                               Active
                                time
  LCD COMx signal:            |<---->|          
  ----------------            |     /          Period
   Vdd  __     ___            |   /           |<----->|                        _
              |   |           |   |           |       |                       |
  Vdd/2 __    |   |    ___     ___     ___     ___     ___     ___            |
              |   |   |   |   |   |   |   |   |   |   |   |   |   |           |
   GND  __ ___|   |___|   |___|   |___|   |___|   |___|   |___|   |___________|
             CC2 CC1 CC2 CC1 CC2 CC1 CC2 CC1 CC2 CC1 CC2 CC1 CC2 CC1 CC2 CC1 CC2
              |       |       |                                               |
              |       |       |               |   |   |   |                   |
              |      /         \              |   |   |   |                   |                       
              |     |<-3.67ms ->|             |   |   |   |                   |
              |                               |   |   |   |                   |
               _______         _______         ___|___    |    _______         _
   CC2     ___|   1   |___2___|   3   |___4___|   5   |___6___|   7   |___8___|
                                                  |       |                  
              |    _______         _______        |_______|        _______    |
   CC1     _______|       |_______|       |_______|       |_______|       |_____
              |                                                               |
              |<------------- One full cycle = 29.36ms (34Hz) --------------->|       
  */

  /* Output Compare Timing Mode configuration: Channel1 */
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Toggle;
  TIM_OCInitStructure.TIM_Pulse = 0x00;   /* CC1 */ 
  TIM_OCInitStructure.TIM_OutputState = ENABLE;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
  TIM_OC1Init(TIMER_LCD, &TIM_OCInitStructure);

  /* Output Compare Timing Mode configuration: Channel2 */
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Toggle;
  TIM_OCInitStructure.TIM_Pulse = LCD_DefaultContrast;   /* CC2 */
  TIM_OCInitStructure.TIM_OutputState = ENABLE;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
  TIM_OC2Init(TIMER_LCD, &TIM_OCInitStructure);  

  /* TIM IT CC1 and CC2 enable */
  TIM_ITConfig(TIMER_LCD, TIM_IT_CC1 | TIM_IT_CC2 , ENABLE);

  /* TIMx enable counter */
  TIM_Cmd(TIMER_LCD, ENABLE);
}



/**
  * @brief  Configure the nested vectored interrupt controller.
  * @param ne.          
  * @retval : None.
  */
void NVIC_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;

  /* Setting the priority grouping bits length */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
  
  /* Enable the TIM_LCD global Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM_LCD_IRQChannel;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = LCD_Priority_Value;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure); 
  
  /* Enable the EXTI0 Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure); 	
}



/**
  * @brief  Configures the different GPIO ports.
  * @param None.
  * @retval : None.
  */
void GPIO_Configuration(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  
  /* GPIOs initialization: all segments and common lines are set as out PP and
     reset to 0  */
  LCD_GPIO_Init();

  /* Configure unused GPIOs as analog input to reduce power consumption */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
  GPIO_Init(CommonLines_Port, &GPIO_InitStructure);

  /* Configure PA.00 as input floating (EXTI Line0) for LCD On/Off button */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 
  GPIO_Init(GPIOA, &GPIO_InitStructure);	  
  
  /* Configure LCD_BiasPlus as Out Push-Pull */
  GPIO_InitStructure.GPIO_Pin = LCD_BiasPlus_Pin;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(CommonLines_Port, &GPIO_InitStructure);  
}



/**
  * @brief  Configures EXTI Line9.
  * @param None.
  * @retval : None.
  */
void EXTI_Configuration(void)
{
  EXTI_InitTypeDef EXTI_InitStructure;
  
  /* Connect EXTI Line0 to PA.00 */
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource0);

  /* Configure EXTI Line0 to generate an interrupt on falling edge */  
  EXTI_InitStructure.EXTI_Line = EXTI_Line0;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);
}    
 
/**
  * @}
  */ 


/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
